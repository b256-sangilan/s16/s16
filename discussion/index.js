// console.log("Hello Thursday!");

// [SECTION] Arithmetic Operators

let x = 1397;
let y = 7831;

// Addition Operator
let sum = x + y;
console.log("Result of addition Operator: " + sum);

// Subtraction Operator
let difference = x - y;
console.log("Result of Subtraction Operator: " + difference);

// Multiplication Operator
let product = x * y;
console.log("Result of Multiplication Operator: " + product);

// Division Operator
let qoutient = x / y;
console.log("Result of Division Operator: " + qoutient);

// Modulo Operator
let modulo = x % y;
console.log("Result of Modulo Operator: " + modulo);

// [SECTION] Assignment Operators

// Basic Assignment Operator (=)
	// The assignment operator adds the value of the right operand to a variable and assigns the result to the variable
let assignmentNumber = 8;

// Addition Assignment Operator (+=)
assignmentNumber = assignmentNumber + 2;
console.log("Result of Addition Assignment Operator is: " + assignmentNumber);
// assignmentNumber = 10

// Shorthand Assignment Operator
assignmentNumber += 2;
console.log("Result of Addition Assignment Operator is: " + assignmentNumber);

// Subtraction Assignment Operator (+=)
assignmentNumber -= 2;
console.log("Result of Subtraction Assignment Operator is: " + assignmentNumber);

// Multiplication Assignment Operator (*=)
assignmentNumber *= 2;
console.log("Result of Multiplication Assignment Operator is: " + assignmentNumber);

// Addition Division Operator (/=)
assignmentNumber /= 2;
console.log("Result of Division Assignment Operator is: " + assignmentNumber);

// [SECTION] Multiple Operators and Parenthesis
// When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule


let mdas = 1 + 2 - 3 * 4 / 5; //
console.log("Result of mdas operation: " + mdas);

// The order of operations can be changed by adding parentheses to the logic
// By adding parentheses "()" the order of the operations are changed prioritizing operations inside the parentheses first then following the MDAS rule for the remaining operations 
let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

// [SECTION] Increment and Decrement Operators
	// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;

// The value of "z" is added by a value of one before returning the value and storing it in the variable "increment"
let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

// The value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by one
increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);

// The value of "z" is decreased by a value of one before returning the value and storing it in the variable "decrement"
let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

// The value of "z" is returned and stored in the variable "decrement" then the value of "z" is decreased by one
decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);

// [SECTION] Type Coercion
	/*
    - Type coercion is the automatic or implicit conversion of values from one data type to another
    - This happens when operations are performed on different data types that would normally not be possible and yield irregular results
    - Values are automatically converted from one data type to another in order to resolve operations
*/

let num1 = '10';
let num2 = 12;

let coercion = num1 + num2;
console.log(coercion);
console.log(typeof coercion);

let num3 = 16;
let num4 = 14;

/* 
    - The result is a number
    - This can be proven in the console by looking at the color of the text displayed
    - Blue text means that the output returned is a number data type
*/

let noncoercion = num3 + num4;
console.log(noncoercion);
console.log(typeof noncoercion);

// boolean " true " is also equal to 1
let num5 = true + 1;
console.log(num5);

// boolean " false " is also equal to 0
let num6 = false + 1;
console.log(num6);

// [SECTION] Comparison Operator

let juan = 'Juan';

// Equality Operator (==)
	// checks whether the two operands are equal/have the same content.
	// return a boolean value as the result
	// attempts to CONVERT and COMPARE operands of different data types.

console.log(1 == 1); // true
console.log(1 == 2); // false
console.log(1 == '1'); // true
console.log(0 == false); // true
console.log( 'juan' == 'juan' ); // true
console.log( 'juan'  == juan); // false

// Inequality Operators (!=)
	// checks whether the two operands are not equal/have different content

console.log(1 != 1); // false
console.log(1 != 2); // true
console.log(1 != '1'); // false
console.log(0 != false); // false
console.log( 'juan' != 'juan' ); // false
console.log( 'juan'  != juan); // true

//  Strict Equality Operator ( === )
	// checks whether the two operands are equal/have the same content AND data type

console.log(1 === 1); // true
console.log(1 === 2); // false
console.log(1  === '1'); // false
console.log(0  === false); // false
console.log( 'juan' === 'juan' ); // true
console.log( 'juan' === juan); // false

//  Strict Inequality Operator ( !== )
	// checks whether the two operands are not equal/have the different content AND data type

console.log(1 !== 1); // false
console.log(1 !== 2); // true
console.log(1 !== '1'); // true
console.log(0 !== false); // true
console.log( 'juan' !== 'juan' ); // false
console.log( 'juan' !== juan); // false

// [SECTION] Relation Operators
	// have boolean values as a result

let a = 50;
let b = 65;

// GT or Greater Than Operator (>)
let isGreaterThan = a > b;
// LT or Less Than Operator (<)
let isLessThan = a < b;
// GTE or Greater Than or Equal Operator (>=)
let isGTOrEqual = a >= b;
// LTE  Less Than or Equal Operator (<=)
let isLTorEqual = a <= b;

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTOrEqual);
console.log(isLTorEqual);

let numString = '30';
console.log(a > numString); // forced coercion
console.log(a <= numString);

let str = 'twenty';
console.log(b >= str); // str = NaN (Not a Number)

// Logical Operator
	// boolean values are returned as result
let isLegalAge = true;
let isRegistered = false;

// AND Operator (&&)
	// return true if ALL operands are true
let allRequirements = isLegalAge && isRegistered; // allREequirements = true && false = FALSE
console.log("Result of Logical AND Operator: " + allRequirements);

// OR Operator (||)
	// return true if atleast one operands is true
let someRequirements = isLegalAge || isRegistered; // someRequirements = true || false = TRUE
console.log("Result of Logical AND Operator: " + someRequirements);

// Not Operator (!)
	// return the opposite value
let someRequirementsNotMet = !isRegistered; // someRequirementsNotMet !false = TRUE
console.log("Result of Logical AND Operator: " + someRequirementsNotMet);